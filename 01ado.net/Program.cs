﻿using System;
using System.Data;
using System.Data.SqlClient;

namespace UkmaAdoNet01
{
	public class Program
	{
		public static void Main(string[] args)
		{
			CreateCommand("CREATE DATABASE Companies(id integer);", "DataSource=test.db; Version=3;");
		}

		private static void CreateCommand(string queryString, string connectionString)
		{
			using (SqlConnection connection = new SqlConnection(connnectionString))
			{
				SqlCommand command = new SqlCommand(queryString, connection);
				command.Connection.Open();
				command.ExecuteNonQuery();
			}
		}
	}
}
